﻿using UnityEngine;
using USharp;
using System;
using System.IO;
using System.Collections.Generic;

public class GameCore : USharp.GameClass
{
    public USharp.DynamicLineRenderer lineDrawer;
    public USharp.DynamicMeshRenderer meshDrawer;


    public sealed class Strings
    {
        public const string cargo1Pref = "cargo1Pref";
        public const string vehicle1Pref = "vehicle1Pref";
        public const string Vehicle1Body = "Vehicle1Body";
        public const string roadEdgeCollider = "rdCldr";
    }
    
    public override void OnStart()
    {
        
        base.OnStart();

        lineDrawer = new DynamicLineRenderer(1024);
        meshDrawer = new DynamicMeshRenderer(1024 * 3);
        
    }
    
    
    public override void GameFixedUpdate()
    {
        base.GameFixedUpdate();

    }

    public override void GameStep()
    {
        lineDrawer.Begin();
        meshDrawer.Begin();

        lineDrawer.SetColor(1, 0, 0, 1);
        lineDrawer.DrawLine(0, 1, 0, 1, 2, 0);

        lineDrawer.Finish();
        lineDrawer.Render();

        meshDrawer.SetColor(1, 0, 0, 1);
        meshDrawer.DrawTriangle(0, 0, 0, 1, 1, 0, 1, 0, 0);

        meshDrawer.Finish();
        meshDrawer.Render();
    }
}